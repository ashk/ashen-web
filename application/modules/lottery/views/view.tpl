{if $hasExpired or $isFinalized }
    <div class="missing-content">
        <p>Participation a cet Edition de Lotterie fermer/p>
    </div>
    <table class="nice_table lottery-winners">
        <tr>
            <td colspan="2">Gagnants</td>
        </tr>
        {foreach from=$winners item=winner}
            <tr>
                <td width="10%">{$winner.rank}</td>
                <td width="90%">{$winner.anchor}</td>
            </tr>
        {/foreach}
    </table>
{else}
        {if not $participant}
            <!-- subscribe form -->
            <div class="subscribe-form">
                <form id="subscribe-form">
                    <label for="character-guid">Selectionner votre Personnage</label>
                    <select id="character-guid">
                        <option value="0">-- Choisi --</option>
                        {foreach from=$characters item=character}
                            <option data-realm="{$character.realm_id}" value="{$character.guid}">{$character.name} ({$character.realm_name})</option>
                        {/foreach}
                    </select>
                    <input type="hidden" id="account-id" value="{$user_id}">
                    <input type="submit" value="Participer">
                    <br/>
                    <div class="helptext">* Vous pouvez participer a la lotterie avec n'importe quel personnage.</div>
                    <div class="helptext">* Une fois inscrit, vous ne pouvez plus participer sur un autre personnage.</div>
                    <div class="helptext">* Vous ne pouvez pas participer avec plusieurs comptes en m�me temps sour peine de bannissement.</div>
                </form>
            </div>
        {/if}
        <div id="subscribed-character" style="{if not $participant}display: none{/if}">
            <h2>Vous avez inscrit votre personnage:
                <a><span id="current-subscribed-character" class="">
                    {foreach from=$characters item=character}
                        {if $character.guid == $participant->character_guid }
                            {$character.name} ({$character.realm_name})
                        {/if}
                    {/foreach}
                </span></a>
            </h2>
            Les gagnants serront choisir au hasard dans<a>{$expiresIn}</a> a partir de maintenant !
        </div>

{/if}

<input type="hidden" id="lottery-id" value="{$lottery->id}">
<!-- general lottery edition data -->
<table class="nice_table lottery-summary">
    <tbody>
        <tr>
            <td colspan="2">Details</td>
        </tr>
        {if $lottery->vp || $lottery->dp}
        <tr class="subscription-fee">
            <td width="30%">Participer !</td>
            <td width="70%">{$cost}</td>
        </tr>
        {/if}
        <tr>
            <td width="30%">Date de commencement</td>
            <td width="70%">{date('d M Y, h:i a', strtotime($lottery->created))}</td>
        </tr>
        <tr>
            <td width="30%">Date de Fin</td>
            <td width="70%">{date('d M Y, h:i a', strtotime($lottery->end))}</td>
        </tr>
        <tr>
            <td width="30%">Recompenses</td>
            <td width="70%">{$lottery->rewards}</td>
        </tr>
        <tr>
            <td width="30%">Nombres de Gagnants</td>
            <td width="70%">{$lottery->winners_count}</td>
        </tr>

        <tr>
            <td width="30%">Participation du Personnage</td>
            <td width="70%">
                <span id="subscribed-characters"></span>
            </td>
        </tr>



    </tbody>
</table>
