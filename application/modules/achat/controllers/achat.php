<?php



class Achat extends MX_Controller
{
	function __construct()
	{
		//Call the constructor of MX_Controller
		parent::__construct();
		
		//Make sure that we are logged in
		$this->user->userArea();

		$this->load->config('achat');
	}
	
	public function index()
	{
		requirePermission("view");

		$this->template->setTitle(lang("achat_title", "achat"));

		$achat_starpa = $this->config->item('achat_starpa');
		$achat_paypal = $this->config->item('achat_paypal');
		$achat_paygol = $this->config->item('achat_paygol');
		
		$user_id = $this->user->getId();
		
		$data = array(
			"achat_paypal" => $achat_paypal, 
			"achat_paygol" => $achat_paygol,
			"achat_starpa" => $achat_starpa,
			"user_id" => $user_id,
			"server_name" => $this->config->item('server_name'),
			"currency" => $this->config->item('donation_currency'),
			"currency_sign" => $this->config->item('donation_currency_sign'),
			"multiplier" => $this->config->item('donation_multiplier'),
			"multiplier_paygol" => $this->config->item('donation_multiplier_paygol'),
			"url" => pageURL
		);


		$output = $this->template->loadPage("achat.tpl", $data);

		$this->template->box("<span style='cursor:pointer;' onClick='window.location=\"".$this->template->page_url."ucp\"'>".lang("ucp")."</span> &rarr; ".lang("achat_panel", "achat"), $output, true, "modules/achat/css/achat.css", "modules/achat/js/achat.js");
	}

	public function success()
	{
		$this->user->getUserData();

		$page = $this->template->loadPage("success.tpl", array('url' => $this->template->page_url));

		$this->template->box(lang("achat_thanks", "achat"), $page, true);
	}
}
