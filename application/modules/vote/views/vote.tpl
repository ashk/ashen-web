<table id="vote" class="nice_table" cellspacing="0" cellpadding="0">
	<tr>
		<td width="30%">{lang("topsite", "vote")}</td>
		<td width="30%">{lang("value", "vote")}</td>
		<td width="40%">&nbsp;</td>
	</tr>
<s><h2 align = "center"><p style = "color:white;"><b> <span style = "color:#D9D95D;">1 Vote = 5 Ashen Points !</span> Pensez à valider le captcha RPG-Paradize ! </b></p></h2></s><br>
<h2 align = "center"><p style = "color:white;"><b> <span style = "color:#D9D95D;">1 Vote = 10 Ashen Points !</span><span style ="color:red;"> Semaine Spéciale : Event VOTE ! </span></b></p></h2>
	{if $vote_sites}
	{foreach from=$vote_sites item=vote_site}
		<tr>
			<td>{if $vote_site.vote_image}<img src="{$vote_site.vote_image}" />{else}{$vote_site.vote_sitename}{/if}</td>
			<td>{$vote_site.points_per_vote}
				{if $vote_site.points_per_vote > 1}
					{lang("voting_points", "vote")}
				{else}
					{lang("voting_point", "vote")}
				{/if}
			</td>
			<td id="vote_field_{$vote_site.id}">
				{if $vote_site.canVote}
					{form_open("vote/site/", $formAttributes)}
						<input type="hidden" name="id" value="{$vote_site.id}" />
						<input type="submit" onClick="Vote.open({$vote_site.id}, {$vote_site.hour_interval});" value="{lang("vote_now", "vote")}"/>
					</form>
				{else}
					{$vote_site.nextVote} {lang("remaining", "vote")}
				{/if}
			</td>
		</tr>
	{/foreach}
	{/if}
</table>

<div class="firefox" style="display:none;text-align:center;padding:10px;font-style:italic;">
	Please allow pop-up windows from this website to be able to vote.
</div>