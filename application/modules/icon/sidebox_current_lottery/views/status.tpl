{if $isFinalized}
    <!-- is finalized, display winners -->
    Edition <b>{$edition}</b> is finished.  <br/>
    <table width="100%">
        {foreach from=$winners item=winner}
        <tr>
            <td>Rank {$winner.rank}</td>
            <td>
                {$winner.anchor}
            </td>
        </tr>
        {/foreach}
    </table>
{else}
    <!-- not finalized, display statistics -->
    <table width="100%">
        <tr>
            <td>Edicion actual</td>
            <td><a href="{$url}lottery/edition/{$lottery->id}">{$edition}</a></td>
        </tr>
        <tr>
            <td>Tiempo restante</td>
            <td>{$expiresIn}</td>
        </tr>
        {if $lottery->vp || $lottery->dp}
        <tr>
            <td>Costo de suscripcion</td>
            <td>{$cost}</td>
        </tr>
        {/if}
        <tr>
            <td>Los suscriptores(hasta ahora)</td>
            <td>{$participantsCount}</td>
        </tr>
        <tr>
            <td>Su estado</td>
            <td>
                {if $isSubscribed}
                    <span style="color: green">subscribed</span>
                {else}
                    not subscribed
                {/if}
            </td>
        </tr>
        {if not $isSubscribed}
            <tr>
                <td style="text-align: center;" colspan="2">
                    <a data-direct="1" href="{$url}lottery/edition/{$lottery->id}">Haga clic aqui para suscribirse</a>
                </td>
            </tr>
        {/if}
    </table>
{/if}
