{if $isFinalized}
    <!-- is finalized, display winners -->
    Edition <b>{$edition}</b> is finished.  <br/>
    <table width="100%">
        {foreach from=$winners item=winner}
        <tr>
            <td>Rank {$winner.rank}</td>
            <td>
                {$winner.anchor}
            </td>
        </tr>
        {/foreach}
    </table>
{else}
    <!-- not finalized, display statistics -->
    <table width="100%">
        <tr>
            <td>Edition Actuel</td>
            <td><a href="{$url}lottery/edition/{$lottery->id}">{$edition}</a></td>
        </tr>
        <tr>
            <td>Temp restant</td>
            <td>{$expiresIn}</td>
        </tr>
        {if $lottery->vp || $lottery->dp}
        <tr>
            <tdCo�t de Participation</td>
            <td>{$cost}</td>
        </tr>
        {/if}
        <tr>
            <td>Participants (pour l'instant)</td>
            <td>{$participantsCount}</td>
        </tr>
        <tr>
            <td>Etat Actuel</td>
            <td>
                {if $isSubscribed}
                    <span style="color: green">Inscrit !/span>
                {else}
                    Vous n'etes pas Inscrit !
                {/if}
            </td>
        </tr>
        {if not $isSubscribed}
            <tr>
                <td style="text-align: center;" colspan="2">
                    <a data-direct="1" href="{$url}lottery/edition/{$lottery->id}">Cliquez ici pour vous inscrire a la Lotterie</a>
                </td>
            </tr>
        {/if}
    </table>
{/if}
