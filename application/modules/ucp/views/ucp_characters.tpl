{if $characters > 0}
<div class="ucp_divider"></div>
<section id="ucp_characters">
	{foreach from=$realms item=realm}
		{if $realm->getCharacterCount() > 0}
			<h1>{$realm->getName()}</h1>
			<section class="statistics_top_hk" style="display:block;">
			<table class="nice_table" cellspacing="0" cellpadding="0">
                    <tr>
                    	<td width="15%" align="center">Nom</td>
                        <td width="15%" align="center">Niveau</td>
                        <td width="15%" align="center">Race</td>
                        <td width="15%" align="center">Classe</td>
                    </tr>
			{foreach from=$realm->getCharacters()->getCharactersByAccount() item=character} 
						<tr>
							<td width="15%" align="center">{$character.name}</td>
				            <td width="15%" align="center">{$character.level}</td>
                            <td width="15%" align="center"><img src="{$url}application/images/stats/{$character.race}-{$character.gender}.gif" /></td>
                            <td width="15%" align="center"><img src="{$url}application/images/stats/{$character.class}.gif" /></td>
                        </tr>
            
			{/foreach}
			</table>
		</section>
		{/if}
	{/foreach}
	<div class="clear"></div>
</section>
{/if}