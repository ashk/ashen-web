<?php

/**
 * Set the links of the UCP buttons.
 * Leave empty to disable the button!
 */
$config['ucp_vote'] = "vote";
$config['ucp_achat'] = "achat";
$config['ucp_store'] = "store";
$config['ucp_settings'] = "ucp/settings";
$config['ucp_expansion'] = "";
$config['ucp_teleport'] = "teleport";
$config['ucp_gm'] = "gm";
$config['ucp_admin'] = "admin";