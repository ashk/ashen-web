<?php

/**
 * Note to module developers:
 * 	Keeping a module specific language file like this
 *	in this external folder is not a good practise for
 *	portability - I do not advice you to do this for
 *	your own modules since they are non-default.
 *	Instead, simply put your language files in
 *	application/modules/yourModule/language/
 *	You do not need to change any code, the system
 *	will automatically look in that folder too.
 */

$lang['register'] = "S'Enregistrer";
$lang['username_limit_length'] = "Le nom de compte doit contenir entre 4 et 32 caractères";
$lang['username_limit'] = "Le nom de compte doit contenir uniquement des lettres et des chiffres";
$lang['username_not_available'] = "Le nom de compte est invalide";
$lang['email_invalid'] = "L'email doit être valide";
$lang['password_short'] = "Le mot de passe doit faire plus de 6 caractères";
$lang['password_match'] = "Les mots de passes ne sont pas les mêmes";
$lang['email_not_available'] = "L'Email est invalide";
$lang['confirm_account'] = "Confirmer la création de comptes s'il vous plait";
$lang['created'] = "Votre compte  été créé!";
$lang['invalid_key'] = "Mauvais captcha!";
$lang['invalid_key_long'] = "La clé d'activation est invalide!";
$lang['the_account'] = "Le compte";
$lang['has_been_created'] = "a été créé. Confirmer l'email reçu dans votre boite!";
$lang['creating_account_forum'] = "Creating account on the forum, please wait...";
$lang['has_been_created_redirecting'] = "has been created. You are being redirected to the";
$lang['user_panel'] = "Panel Utilisateur";
$lang['username'] = "Nom de Compte";
$lang['email'] = "Email";
$lang['password'] = "Mot de passe";
$lang['confirm'] = "Confirmez le mot de passe";
$lang['expansion'] = "Extension";
$lang['submit'] = "Creation du compte!";