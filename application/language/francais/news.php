<?php

/**
 * Note to module developers:
 * 	Keeping a module specific language file like this
 *	in this external folder is not a good practise for
 *	portability - I do not advice you to do this for
 *	your own modules since they are non-default.
 *	Instead, simply put your language files in
 *	application/modules/yourModule/language/
 *	You do not need to change any code, the system
 *	will automatically look in that folder too.
 */

$lang['type_comment'] = "Mettre un commentaire...";
$lang['characters'] = "caractères";
$lang['submit'] = "Ajouter le commentaire";
$lang['log_in'] = "Connectez vous pour commenter";
$lang['view_profile'] = "Voir le profil";
$lang['comments'] = "Commentaires";
$lang['posted_by'] = "Publié par";
$lang['on'] = "on";