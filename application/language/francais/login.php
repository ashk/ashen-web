<?php

/**
 * Note to module developers:
 * 	Keeping a module specific language file like this
 *	in this external folder is not a good practise for
 *	portability - I do not advice you to do this for
 *	your own modules since they are non-default.
 *	Instead, simply put your language files in
 *	application/modules/yourModule/language/
 *	You do not need to change any code, the system
 *	will automatically look in that folder too.
 */

$lang['log_in'] = "Connexion";
$lang['user_doesnt_exist'] = "Le compte n'existe pas";
$lang['password_doesnt_match'] = "Le mot de passe est incorrecte";
$lang['username'] = "Compte";
$lang['password'] = "Mot de passe";
$lang['remember_me'] = "Coche cette case pour rester connecté";
$lang['remember_me_short'] = "Se souvenir de moi";
$lang['lost_your_password'] = "Avez-vous perdu votre mot de passe?";