<?php

/**
 * English language support
 * @author Jesper Lindström
 */

/**
 * 2-letter language abbreviation, used for flag icons
 * @see http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
 */
$lang['abbreviation'] = "fr";

// User panel abbreviation
$lang['ucp'] = "UCP";

/**
 * ====================================
 * Time identifiers
 * ====================================
 */

// Singular forms
$lang['month'] = "mois";
$lang['day'] = "jour";
$lang['hour'] = "heure";
$lang['minute'] = "minute";
$lang['second'] = "seconde";

// Plural forms
$lang['months'] = "mois";
$lang['days'] = "jours";
$lang['hours'] = "heures";
$lang['minutes'] = "minutes";
$lang['seconds'] = "secondes";

// Permission errors
$lang['denied'] = "Permission refusée";
$lang['must_be_signed_in'] = "Vous devez être connecté pour voir cette page !";
$lang['already_signed_in'] = "Vous êtes déjà connecté";
$lang['no_permission'] = "Vous n'avez pas la permission de voir cette page.";
$lang['click_to_sign_in'] = "Cliquez ici pour vous connecter.";

// Misc
$lang['reason'] = "Raison:";
$lang['banned'] = "Banni";
$lang['close_tool'] = "Close tool";
$lang['offline'] = "Hors ligne";