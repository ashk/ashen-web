<?php

/**
 * Note to module developers:
 * 	Keeping a module specific language file like this
 *	in this external folder is not a good practise for
 *	portability - I do not advice you to do this for
 *	your own modules since they are non-default.
 *	Instead, simply put your language files in
 *	application/modules/yourModule/language/
 *	You do not need to change any code, the system
 *	will automatically look in that folder too.
 */

$lang['donate_title'] = "Acheter des Ashen Point";
$lang['donate_panel'] = "Panel d'donate";
$lang['donate_thanks'] = "Merci de votre donate!";
$lang['donate_success'] = "Merci de soutenir notre serveur!";
$lang['paypal'] = "PayPal";
$lang['paygol'] = "StarPass";
$lang['donation_for'] = "donate pour";
$lang['dp'] = "Ashen-Points";
$lang['for'] = "pour"; // as in "X Donation points >for< $Y"
$lang['pay_paypal'] = "Payer avec Paypal";
$lang['pay_paygol'] = "Payer avec StarPass";
$lang['no_methods'] = "S'il vous plait configurer votre méthode de payement";